#Node on FHIR
This a modification of the official HL7 FHIR Release taken from their [download section](https://www.hl7.org/fhir/downloads.html).
##Run
* Install Node
* Install MongoDB

Then cd into the project's root and  install dependencies via:
```shell
npm install
```
Then start the server with
```shell
node server
```
Or, by using
```shell
nodemon server
```
(All project files will get watched and the server restarts automatically on change.)


Per default, the server listens on Port 3000. You can access it via `curl` (see below) or your browser at http://localhost:3000.



##Changes made
The schemas in `questionnaire.js` and `testscript.js` had to be modified because there are `options` and `path` defined, which are [reserved keys](http://mongoosejs.com/docs/api.html#schema_Schema.reserved). See Github Issue  [#1760](https://github.com/Automattic/mongoose/issues/1760).

Access and handling of calls to the REST API have also been adapted to match the [specification](http://www.hl7.org/fhir/http.html).
##REST API
###READ
The latest resource of a type can be read by id with a GET request to:
`http://localhost:3000/[type]/[id]`
e.g.:
~~~~
curl -i -X GET http://localhost:3000/patient/568c0a52a61511f8142f6b69
~~~~

Successful request get status `200` together with the resource returned.

Requests to unknown resources get a `404` returned.

Requests to deleted resources get a `410` returned.
###VREAD
The URL for VREAD has been changed (now includes *_history*).

Specific versions of a resource can be read with a GET request to:
`http://localhost:3000/[type]/[id]/_history/[vid]`

e.g.:
~~~~
curl -i -X GET http://localhost:3000/patient/568cff79cd4e6d8f2e6442b2/_history/2
~~~~

See CREATE for htttp status codes.

Requests to versions that do not exist also get a `404` returned.
###LIST
A GET request to a type without specifying an id returns all resources of a type:
`http://localhost:3000/[type]/`
e.g.:
~~~~
curl -i-X GET http://localhost:3000/patient/
~~~~

###CREATE
In the reference implementation, resources were created with a GET request to *resourceType*/**create**.

This was changed to a POST request [as specified](https://www.hl7.org/fhir/http.html#create) to:
`http://localhost:300/[type]/`
e.g.:
```shell
curl -i -X POST -H "Content-Type: application/json" http://localhost:3000/patient --data "{\"name\": [{ \"family\": \"Burns\", \"given\": \"Montgomery\" }]}"
```

Successful request get `201` code returned and a link to the new resources in the header's location tag.


If the given properties could not be parsed, or an id was specified, a status `400` is returned.

If the type is not supported a a status `404` is returned.

A Status `422` response is currently not returned.

###UPDATE
A resource may be updated by a PUT request to the according resource, identfied by it's id:
`http://localhost:3000/[type]/[id]`
e.g.:
```shell
curl -X PUT -H "Content-Type: application/json" http://localhost:3000/patient/568cff79cd4e6d8f2e6442b2/ --data "{\"name\": [{ \"prefix\" : \"Mr\",  \"family\": \"Burns\", \"given\": \"Montgomery\" }]}"
```
**Note**: deep object changes are not supported. Like above, if you want to update a value (here: *prefix*), you also need to specify *family* and *given* again, because those values are inside the name object.

If you would just update the gender, you don't need to provide any other properties, because gender is a top-level property:
```shell
curl -X PUT -H "Content-Type: application/json" http://localhost:3000/patient/568cff79cd4e6d8f2e6442b2/ --data "{\"gender\": \"male\" }]}"
```

*This might be the PATCH behaviours mentioned in the[ specification's note](http://www.hl7.org/fhir/http.html#update).*

Currently the id of the request body is not checked for an id element identical to the id in the URL. So you won't get a `400` when omitting the id in the body.


Successful updates request get a status `200` returned.


**Create with Put**

You may also create a resource with a PUT request if you provide an id.

Then you get either a status `201` on successful creation.


If the given properties could not be parsed status `400` is returned.

If the type is not supported a a status `404` is returned.

A Status `422` response is currently not returned.

###DELETE
A resource can be deleted by a DELETE rquest to the according resource, identfied by it's id:
`http://localhost:3000/[type]/[id]`
e.g.:
~~~~
curl -i -X DELETE http://localhost:3000/patient/568c0a52a61511f8142f6b69
~~~~
Deletion means a empty resource is pushed to the patient's resource history. Old version may still be read, but the most recent version is just empty.


Upon successful deletion, a status `204` is returned.

Deletion of a already deleted resource also returns a `204`.

Denying deletion of a resource is currently not supported, so `405` and `409` won't be returned.


##MongoDB
###Connect DB

    mongo fhir

###Delete/Reset Database
Connect to DB as described above and then drop the database with:

    db.dropDatabase()

After the next server start, it will be automatically recreated.

##Test
Run tests with
```shell
npm test
```