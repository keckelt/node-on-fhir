var mongoose = require('mongoose');
var _ = require('underscore');
var async = require('async');
var ResourceHistory = mongoose.model('ResourceHistory');


exports.load = function(req, res, id, vid, next) {
  if (req.resourceHistory) {
    if(vid !== null){
      req.resourceHistory.getVersion(vid, function(err, resource) {
        req.resource = resource;
        next(resourceresource);
      });
    } else {
      req.resourceHistory.findLatest(function(err, resource) {
        req.resource = resource;
        next(resource);
      });
    }
  } else {
      ResourceHistory.findById(id, function(rhErr, resourceHistory) {
          if (rhErr) {
              next(rhErr);
              return;
          }

        if(resourceHistory !== undefined && resourceHistory !== null) {
            req.resourceHistory = resourceHistory;
            if(vid === undefined || vid === null) {
                //Return latest
                req.resourceHistory.findLatest(function(err, resource) {
                    if(err) {
                        next(err);
                    } else {
                        req.resource = resource;
                        next(resource);
                    }
                });
            } else {
                //return given version
                req.resourceHistory.getVersion(vid, function(err, resource) {
                    if(err) {
                        next(err);
                    } else {
                        req.resource = resource;
                        next(resource);
                    }
                });
            }
        } else {
            var err = new Error("No resource with id "+id);
            err.code = "RES_NOT_FOUND";
            next(err);
        }
    });
  }
};


exports.create = function(req, res, modelName) {
  var ResourceModel = mongoose.model(modelName);
  var resource = new ResourceModel(req.body);

  resource.save(function(err, savedResource) {
    if(err) {
      res.sendStatus(500);
    } else {
        var resourceHistory = new ResourceHistory({resourceType: modelName});
        if(req.params.id !== undefined)
        {
            resourceHistory._id = req.params.id;
        }
        resourceHistory.addVersion(savedResource._id);
        resourceHistory.save(function(rhErr, savedResourceHistory){
        if (rhErr) {
          res.sendStatus(500);
        } else {
          res.set('Location', ("http://localhost:3000/"+modelName.toLowerCase()+"/" + resourceHistory._id));
          res.sendStatus(201);
        }
      });
    }
  });
};

exports.update = function(req, res, modelName) {
    var ResourceModel = mongoose.model(modelName);

    //req.resource is a mongoose document -> to object to create a new document
    var resourceObj = req.resource.toObject();
    delete resourceObj._id;
    delete req.body._id;
    resourceObj = _.extend(resourceObj, req.body); //does not deep extend (e.g. you have the set the whole name with given and family and cant just update one of it)

    var resource = new ResourceModel(resourceObj);

    resource.save(function(err, savedResource) {
        if(err) {
          res.sendStatus(500);
        } else {
          var resourceHistory = req.resourceHistory;
          resourceHistory.addVersion(savedResource._id);
          resourceHistory.save(function(rhErr, savedResourceHistory) {
            if (rhErr) {
              res.sendStatus(500);
            } else {
              res.sendStatus(200);
            }
          });
        }
    });
};

exports.destroy = function(req, res) {
    var resourceHistory = req.resourceHistory;

    resourceHistory.addVersion("0000"+"0000"+"0000"+"0000"+"0000"+"0000");
    resourceHistory.save(function(rhErr, savedResourceHistory) {
        if (rhErr) {
          res.sendStatus(500);
        } else {
          res.sendStatus(204);
        }
      });
};

exports.list = function(req, res, modelName) {

  var content = {
    title: "Search results for resource type "+modelName,
    id: "http://localhost:3000/"+modelName.toLowerCase(),
    totalResults: 0,
    link: {
      href: "http://localhost:3000/"+modelName.toLowerCase(),
      rel: "self"
    },
    updated: new Date(Date.now()),
    entry: []
  };

    ResourceHistory.find({resourceType: modelName}, function (rhErr, histories) {
    if (rhErr) {
      return next(rhErr);
    }
    var counter = 0;
    async.forEach(histories, function(history, callback) {
      counter++;
      content.totalResults = counter;
      history.findLatest( function(err, resource) {
        var entrywrapper = {
          title: modelName+" " + history._id + " Version " + history.versionCount(),
          id: "http://localhost:3000/"+modelName.toLowerCase()+"/" + history._id,
          link: {
            href: "http://localhost:3000/"+modelName.toLowerCase()+"/" + history._id + "/_history/" + history.versionCount(),
            rel: "self"
          },
          updated: history.lastUpdatedAt(),
          published: new Date(Date.now()),
          content: resource
        };
        content.entry.push(entrywrapper);
        callback();
      });
    }, function(err) {
        res.send(content);
    });
  });
};

