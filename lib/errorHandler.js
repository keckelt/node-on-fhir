//Answer Requests to missing modules to with a 404
exports.missingModuleHandler = function missingModuleHandler(err, req, res, next) {
    if(err.code==="MODULE_NOT_FOUND")
    {
        res.status(404).send("No resource with name: '"+err.message.substring(err.message.lastIndexOf('/')+1)+"."); //Request to undefined module = Bad Request
    }else
    {
        next(err); //Pass error to next handler
    }
}

exports.invalidIdHandler = function missingModuleHandler(err, req, res, next) {
    if(err.code==="INVALID_OBJECT_ID")
    {
        res.status(400).send(err.message); //Request with inalid resource id = Bad Request
    }else
    {
        next(err); //Pass error to next handler
    }
}

exports.parseErrorHandler = function parseErrorHandler(err, req, res, next) {
    if(err.constructor.name === "SyntaxError")
    {
        if(err.statusCode !== undefined)
        {
            res.sendStatus(err.statusCode);
        } else
        {
            res.sendStatus(400);
        }
    }else
    {
        next(err);
    }
}

//Simply log the error to console
exports.logErrors = function logErrors(err, req, res, next) {
  console.error(err);
  next(err); //pass error to next handler
}


// -------------------------------------------



// Insert new error Handlers here



// -------------------------------------------

//Handle each error with 500
//Add as last errorHandler
exports.errorHandler = function errorHandler(err, req, res, next) {
  res.sendStatus(500); //Unhandled Error = Internal Server Error
}