// Copyright (c) 2011+, HL7, Inc & The MITRE Corporation
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification, 
// are permitted provided that the following conditions are met:
// 
//  * Redistributions of source code must retain the above copyright notice, this 
//    list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above copyright notice, 
//    this list of conditions and the following disclaimer in the documentation 
//    and/or other materials provided with the distribution.
//  * Neither the name of HL7 nor the names of its contributors may be used to 
//    endorse or promote products derived from this software without specific 
//    prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
// POSSIBILITY OF SUCH DAMAGE.

// create express object to be returned
var express  = require('express');
var bodyParser = require('body-parser');
var errorHandler = require('../lib/errorHandler');
var mongoose = require('mongoose');

// create app object and setup as exported member of module
var app = express();

//use body parser to read POST request bodies.
app.use(bodyParser.json()); // support json encoded bodies
//app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// root url(not necessary)
app.get('/', function (req, res) {
    console.log("\nGET resource list")
    app.provider.findAll(function (error, collection) {
        res.send(collection);
    });
});

//index for model(not necessary)
app.get('/:model', function (req, res) {
    console.log("\nGET", req.params);

    checkParams(req);
    var controller = require('../app/controllers/supercontroller');
    controller.list(req, res, getModelNameWithCase(req.params.model));
});

//VREAD of a model
//http://www.hl7.org/fhir/http.html#vread
app.get('/:model/:id/_history/:vid', getResource);
//READ of a model
//http://www.hl7.org/fhir/http.html#read
app.get('/:model/:id', getResource);
function getResource (req, res) {
    console.log("\nGET", req.params);

    checkParams(req);
    var controller = require('../app/controllers/supercontroller'); //throws error if model does not exist
    controller.load(req, res, req.params.id, req.params.vid, function (obj) {
        if(obj === null || obj === undefined)
        {
            res.sendStatus(410); //Deleted resource
        } else if (obj.constructor.name.indexOf("Error") >= 0)
        {
            console.log("Got an error: " + obj);
            if(obj.code === "RES_NOT_FOUND" || obj.code === "VER_NOT_FOUND")
            {
                res.sendStatus(404); //resource does not exist
            } else
            {
                res.sendStatus(500); //unhandled error
            }
        } else {
            res.send(req.resource);
        }
    });
}


//CREATE for model (no parameters)
//http://www.hl7.org/fhir/http.html#create
app.post('/:model/', function (req, res) {
   console.log("\nPOST", "params", req.params, "body", req.body);

    checkParams(req);
    if("_id" in req.body || "id" in req.body)
    {
        res.status(400).send("Providing an id is not allowed.");
        return;
    }

   var controller = require('../app/controllers/supercontroller');
   controller.create(req, res, getModelNameWithCase(req.params.model));
});


//UPDATE for model (only id as parameter (no vid))
//http://www.hl7.org/fhir/http.html#update
app.put('/:model/:id', function (req, res) {
   console.log("\nPUT", req.params);

   checkParams(req);
   var controller = require('../app/controllers/supercontroller');


   controller.load(req, res, req.params.id, null, function (obj) {
     if(obj.constructor.name === "Error")
     {
       console.log("Got an error: " + obj)
       if(obj.code === "RES_NOT_FOUND")
       {
           console.log("resource for PUT not found, creating new resource")
           controller.create(req, res, getModelNameWithCase(req.params.model));
       } else
       {
           res.sendStatus(500)
       }
     } else {
       controller.update(req, res, getModelNameWithCase(req.params.model));
     }
   });
});

//DELETE for model
app.delete('/:model/:id/', function (req, res) {
  console.log("\nDELETE", req.params);

  checkParams(req);
  var controller = require('../app/controllers/supercontroller');

  controller.load(req, res, req.params.id, undefined, function(obj) {
      if(obj === undefined || obj === null)
      {
          console.log("resource is already deleted");
          res.sendStatus(204);
      } else  if(obj.constructor.name=="Error")
      {
        console.log("Got an error: " + obj)
        if(obj.code === "RES_NOT_FOUND" )
          {
              res.sendStatus(404);
          }else
          {
            res.sendStatus(500)
          }
      } else {
        controller.destroy(req,res)
      }
  });
});

//404 Handler
app.use(function(req, res, next) {
  res.sendStatus(404);
});

//Error Handler
//Must be added last, after other app.use() and routes calls;
app.use(errorHandler.missingModuleHandler);
app.use(errorHandler.invalidIdHandler);
app.use(errorHandler.parseErrorHandler);
app.use(errorHandler.logErrors);
app.use(errorHandler.errorHandler);

exports.app = app;






///HELPER METHODS

function checkParams(req)
{
    checkModel(req.params.model);

    if(req.params.id !== undefined)
    {
        checkId(req.params.id)
    }
}

//checks if Id is correct
function checkId(id)
{
    if(!id.match(/^[0-9a-fA-F]{24}$/)) {
        console.log("ID does not match")
        var err = new Error("Invalid resource Id: "+id);
        err.code = "INVALID_OBJECT_ID";
        throw err;
    }
}

//Checks if model does exist
function checkModel(modelName)
{
    var index = indexOfCaseInsensitiv(mongoose.modelNames(), modelName);
    if(index<0) {
        var err = new Error("Invalid module name: "+modelName);
        err.code = "MODULE_NOT_FOUND";
        throw err;
    }
}

function getModelNameWithCase(modelName)
{
    var index = indexOfCaseInsensitiv(mongoose.modelNames(), modelName);
    if(index < 0) {
        return null;
    }else {
        return mongoose.modelNames()[index];
    }
}

function indexOfCaseInsensitiv(arr, toCompare)
{
    var index = -1;
    arr.some(function(element, i) {
        if (toCompare === element.toLowerCase()) {
            index = i;
            return true;
        }
    });

    return index;
}
