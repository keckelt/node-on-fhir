#!/bin/bash

echo clear database
mongo fhir --eval "db.dropDatabase()" >/dev/null

echo Test URLS that do not exist:
curl -X GET http://localhost:3000/trololo
echo ""
curl -X GET http://localhost:3000/trololo/11
echo ""
curl -X GET http://localhost:3000/patient/11/history
echo ""
curl -X GET http://localhost:3000/patient/11/_history
echo ""
curl -X GET http://localhost:3000/patient/11/_history/11
echo ""

read -p "Press [ENTER] to continue with resource creation."

echo Create Montgomery Burns
location=$(curl -i -X POST -H "Content-Type: application/json" http://localhost:3000/patient --data "{\"name\": [{ \"family\": \"Burns\", \"given\": \"Montgomery\" }]}" 2>|/dev/null | awk '/^Location:/ { print $2 }' | sed -e 's/[[:cntrl:]]//')
echo He can be found at: ${location}

echo Show Montgomery Burns
curl -X GET ${location}

read -p "Press [ENTER] to add a 'Mr' prefix for Monty Burns."

curl -i -X PUT -H "Content-Type: application/json" ${location} --data "{\"name\": [{ \"prefix\" : \"Mr\",  \"family\": \"Burns\", \"given\": \"Montgomery\" }], \"gender\" : \"male\"}"
curl -X GET ${location}/_history/2

read -p "The old entry is still available, press [ENTER] to display"
curl -X GET ${location}/_history/1

read -p "Let's delete that Mr. Burns because he built his own super duper hospital. Press [ENTER]."
curl -X DELETE ${location}
echo Read his resource again:
curl -X GET ${location}
echo ""
read -p "The old entry (with out Mr prefix) should still be around. Press [ENTER] to check"
curl -X GET ${location}/_history/1

read -p "Hit [ENTER] to create resource with a PUT."
curl -i -X PUT -H "Content-Type: application/json" http://localhost:3000/patient/123456789012345678901234 --data "{\"name\": [{ \"prefix\" : \"Dr\",  \"family\": \"Brown\", \"given\": \"Emmett\" }]}"
