var supertest = require("supertest");
var should = require("should");
var mongo = require('mongoose')
var resourceList = require('./resourceList.js').resourceList;

// This agent refers to PORT where program is runninng.
var server = supertest.agent("http://localhost:3000");


describe("Test Root URL '/'",function(){
  it("GET should return collection of available resources as JSON",function(done){
    // calling home page api
    server
    .get("/")
    .expect("Content-Type",/json/)
    .expect(200)
    .end(function(err,res){
        if(err) return done(err);

        res.body.should.be.ok;
        res.body.should.be.an.Array;
        res.body.should.eql(resourceList); //eql compares content, not reference
        res.body.length.should.equal(resourceList.length);
        done();
    });
  });
});

describe("Test Wrong URLs", function() {
    it("GET should return 404 for non existing models", function(done) {
        server
        .get("/nfoinwfoüaiwn")
        .expect(404)
        .end(function(err,res){
            if(err) return done(err);

            res.body.should.be.empty;
            done();
        });
    });

    it("GET should return 404 for non existing models", function(done) {
        server
        .get("/nfoinwfoüaiwn/1")
        .expect(404)
        .end(function(err,res){
            if(err) return done(err);

            res.body.should.be.empty;
            done();
        });
    });

    it("GET should return 404 for non existing models", function(done) {
        server
        .get("/nfoinwfoüaiwn/012345678901234567891234")
        .expect(404)
        .end(function(err,res){
            if(err) return done(err);

            res.body.should.be.empty;
            done();
        });
    });

    it("GET should return 404 for non existing models", function(done) {
        server
        .get("/nfoinwfoüaiwn/012345678901234567891234/_history/1")
        .expect(404)
        .end(function(err,res){
            if(err) return done(err);

            res.body.should.be.empty;
            done();
        });
    });

    it("POST should return 404 for non existing models", function(done) {
        server
        .post("/nfoinwfoüaiwn")
        .expect(404)
        .end(function(err,res){
            if(err) return done(err);

            res.body.should.be.empty;
            done();
        });
    });

    it("PUT should return 404 for non existing models", function(done) {
        server
        .put("/nfoinwfoüaiwn")
        .expect(404)
        .end(function(err,res){
            if(err) return done(err);

            res.body.should.be.empty;
            done();
        });
    });

    it("DELETE should return 404 for non existing models", function(done) {
        server
        .del("/nfoinwfoüaiwn")
        .expect(404)
        .end(function(err,res){
            if(err) return done(err);

            res.body.should.be.empty;
            done();
        });
    });
});


resourceList.forEach(testResource);
//testResource("patient");

function testResource(resource, index) {
    if(resource==='resource_history') {
        //resource history is the only resource that should not be accessible
        describe("Test resource: "+resource, function(){
          it("should NOT return list of resources",function(done){
            // calling home page api
            server
            .get("/"+resource)
            .expect(404, done);
          });
      });
    } else {
        var numOfResources = 0;

        describe("Test resource: "+resource, function(){
            it("should return list of resources", function(done){
                server
                .get("/"+resource)
                .expect("Content-Type",/json/)
                .expect(200)
                .end(function(err,res){
                    if(err) return done(err);

                    res.body.should.be.ok;
                    res.body.totalResults.should.be.ok;
                    res.body.id.should.eql(server.app+"/"+resource);
                    numOfResources =res.body.totalResults;
                    done();
                });
            });

            it("should return 404 for non exisiting id", function(done) {
                server
                .get("/"+resource+"/"+mongo.Types.ObjectId()) //hope ID does not exist
                .expect(404, done);
            });

            it("should return 400 for too short id", function(done) {
                server
                .get("/"+resource+"/01234567890123456789bb8")
                .expect(400, done);
            });

            it("should return 400 for id with invalid characters", function(done) {
                server
                .get("/"+resource+"/01234567890123456789012z")
                .expect(400, done);
            });

            var newResourceId;

            //CREATE with POST
            it("should create empty resource with POST", function(done){
                server
                .post("/"+resource)
                .expect(201)
                .end(function(err,res){
                    if(err) return done(err);

                    res.body.should.be.empty;
                    res.header.location.should.be.ok;
                    var location = res.header.location;
                    location.should.match(new RegExp("http.*\/"+resource+"\/[0-9a-z]{24}"));
                    newResourceId = location.substring(location.indexOf(resource+"/")+resource.length+1);
                    newResourceId.should.match(/^[0-9a-z]{24}$/);
                    done();
                });
            });

            var newResource;

            //READ
            it("should GET created resouce", function(done){
                server
                .get("/"+resource+"/"+newResourceId)
                .expect("Content-Type",/json/)
                .expect(200)
                .end(function(err,res){
                    if(err) return done(err);

                    res.body.should.be.ok;
                    res.body.should.not.be.empty;
                    newResource = res.body;
                    done();
                });
            });

            //UPDATE
            it("should update created resource with PUT", function(done){
                server
                .put("/"+resource+"/"+newResourceId)
                .send(newResource)
                .expect(200)
                .end(function(err,res){
                    if(err) return done(err);

                    res.body.should.be.empty;
                    done();
                });
            });

            //VREAD v2
            it("should GET newested resouce version", function(done){
                server
                .get("/"+resource+"/"+newResourceId+"/_history/2")
                .expect("Content-Type",/json/)
                .expect(200)
                .end(function(err,res){
                    if(err) return done(err);

                    res.body.should.be.ok;
                    res.body.should.not.be.empty;
                    done();
                });
            });
            //VREAD v1
            it("should GET first resource version", function(done){
                server
                .get("/"+resource+"/"+newResourceId+"/_history/1")
                .expect("Content-Type",/json/)
                .expect(200)
                .end(function(err,res){
                    if(err) return done(err);

                    res.body.should.be.ok;
                    res.body.should.not.be.empty;
                    done();
                });
            });

            //DELETE
            it("should DELETE resource", function(done){
                server
                .del("/"+resource+"/"+newResourceId)
                .expect(204)
                .end(function(err,res){
                    if(err) return done(err);

                    res.body.should.be.empty;
                    done();
                });
            });

            //DELETE
            it("DELETE deleted resource should return 204 aswell", function(done){
                server
                .del("/"+resource+"/"+newResourceId)
                .expect(204)
                .end(function(err,res){
                    if(err) return done(err);

                    res.body.should.be.empty;
                    done();
                });
            });

            //READ Deleted
            it("deleted resource should be empty", function(done){
                server
                .get("/"+resource+"/"+newResourceId)
                .expect(410)
                .end(function(err,res){
                    if(err) return done(err);

                    res.body.should.be.empty;
                    done();
                });
            });

            var createResourceId = mongo.Types.ObjectId();
            //CREATE with PUT
            it("should create resource with given id with PUT", function(done){
                server
                .put("/"+resource+"/"+createResourceId)
                .expect(201)
                .end(function(err,res){
                    if(err) return done(err);

                    res.body.should.be.empty;
                    done();
                });
            });

            //READ
             it("should GET created resource", function(done){
                server
                .get("/"+resource+"/"+createResourceId)
                .expect("Content-Type",/json/)
                .expect(200)
                .end(function(err,res){
                    if(err) return done(err);

                    res.body.should.be.ok;
                    res.body.should.not.be.empty;
                    newResource = res.body;
                    done();
                });
            });

            //READ ALL (numOfResources = +2)
            it("should have two more elements in list of resources", function(done){
                server
                .get("/"+resource)
                .expect("Content-Type",/json/)
                .expect(200)
                .end(function(err,res){
                    if(err) return done(err);

                    res.body.should.be.ok;
                    res.body.totalResults.should.be.ok;
                    res.body.id.should.eql(server.app+"/"+resource);
                    res.body.totalResults.should.equal(numOfResources+2);
                    done();
                });
            });

            //CREATE with id in POST Body
            it("should NOT create resource with id in body of POST", function(done){
                server
                .post("/"+resource+"/")
                .send({"_id" : "123"})
                .expect(400, done);
            });

            //CREATE with unparsable content in POST Body
            it("should NOT create resource with invalid JSON body of POST", function(done){
                server
                .post("/"+resource+"/")
                .send('{"invalid"}')
                .type('json')
                .expect(400, done);
            });

            //CREATE with unparsable content in PUT Body
            it("should NOT create resource with invalid JSON body of PUT", function(done){
                server
                .put("/"+resource+"/"+mongo.Types.ObjectId())
                .send('{"invalid"}')
                .type('json')
                .expect(400, done);
            });

        });
    }
}



